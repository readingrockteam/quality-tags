﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQualityTags
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FjobnoLabel As System.Windows.Forms.Label
        Dim Fjob_nameLabel As System.Windows.Forms.Label
        Dim FpartnoLabel As System.Windows.Forms.Label
        Dim FquantityLabel As System.Windows.Forms.Label
        Dim FdrawnoLabel As System.Windows.Forms.Label
        Dim FdescmemoLabel As System.Windows.Forms.Label
        Dim FjobnoLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQualityTags))
        Me.pd1 = New System.Drawing.Printing.PrintDocument()
        Me.cmdPrint = New System.Windows.Forms.Button()
        Me.DsM2M = New HT_Quality_Tags.dsM2M()
        Me.JomastBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.JomastTableAdapter = New HT_Quality_Tags.dsM2MTableAdapters.jomastTableAdapter()
        Me.TableAdapterManager = New HT_Quality_Tags.dsM2MTableAdapters.TableAdapterManager()
        Me.cbxJO = New System.Windows.Forms.ComboBox()
        Me.txtJobName = New System.Windows.Forms.TextBox()
        Me.txtPartno = New System.Windows.Forms.TextBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.txtDrawno = New System.Windows.Forms.TextBox()
        Me.txtSO = New System.Windows.Forms.TextBox()
        Me.txtJoSearch = New System.Windows.Forms.TextBox()
        Me.chxAuto = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatabaseConnectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.M2MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowM2MConnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PackingSolutionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowPSConnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SelectPrinterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblPrinter = New System.Windows.Forms.Label()
        Me.lblSelectedPrinter = New System.Windows.Forms.Label()
        FjobnoLabel = New System.Windows.Forms.Label()
        Fjob_nameLabel = New System.Windows.Forms.Label()
        FpartnoLabel = New System.Windows.Forms.Label()
        FquantityLabel = New System.Windows.Forms.Label()
        FdrawnoLabel = New System.Windows.Forms.Label()
        FdescmemoLabel = New System.Windows.Forms.Label()
        FjobnoLabel1 = New System.Windows.Forms.Label()
        CType(Me.DsM2M,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.JomastBindingSource,System.ComponentModel.ISupportInitialize).BeginInit
        Me.MenuStrip1.SuspendLayout
        Me.SuspendLayout
        '
        'FjobnoLabel
        '
        FjobnoLabel.AutoSize = true
        FjobnoLabel.Location = New System.Drawing.Point(54, 62)
        FjobnoLabel.Name = "FjobnoLabel"
        FjobnoLabel.Size = New System.Drawing.Size(39, 13)
        FjobnoLabel.TabIndex = 13
        FjobnoLabel.Text = "JO List"
        '
        'Fjob_nameLabel
        '
        Fjob_nameLabel.AutoSize = true
        Fjob_nameLabel.Location = New System.Drawing.Point(38, 89)
        Fjob_nameLabel.Name = "Fjob_nameLabel"
        Fjob_nameLabel.Size = New System.Drawing.Size(55, 13)
        Fjob_nameLabel.TabIndex = 14
        Fjob_nameLabel.Text = "Job Name"
        '
        'FpartnoLabel
        '
        FpartnoLabel.AutoSize = true
        FpartnoLabel.Location = New System.Drawing.Point(27, 115)
        FpartnoLabel.Name = "FpartnoLabel"
        FpartnoLabel.Size = New System.Drawing.Size(66, 13)
        FpartnoLabel.TabIndex = 15
        FpartnoLabel.Text = "Part Number"
        '
        'FquantityLabel
        '
        FquantityLabel.AutoSize = true
        FquantityLabel.Location = New System.Drawing.Point(50, 167)
        FquantityLabel.Name = "FquantityLabel"
        FquantityLabel.Size = New System.Drawing.Size(43, 13)
        FquantityLabel.TabIndex = 17
        FquantityLabel.Text = "Job Qty"
        '
        'FdrawnoLabel
        '
        FdrawnoLabel.AutoSize = true
        FdrawnoLabel.Location = New System.Drawing.Point(23, 193)
        FdrawnoLabel.Name = "FdrawnoLabel"
        FdrawnoLabel.Size = New System.Drawing.Size(70, 13)
        FdrawnoLabel.TabIndex = 18
        FdrawnoLabel.Text = "Mold Number"
        '
        'FdescmemoLabel
        '
        FdescmemoLabel.AutoSize = true
        FdescmemoLabel.Location = New System.Drawing.Point(71, 141)
        FdescmemoLabel.Name = "FdescmemoLabel"
        FdescmemoLabel.Size = New System.Drawing.Size(22, 13)
        FdescmemoLabel.TabIndex = 19
        FdescmemoLabel.Text = "SO"
        '
        'FjobnoLabel1
        '
        FjobnoLabel1.AutoSize = true
        FjobnoLabel1.Location = New System.Drawing.Point(45, 36)
        FjobnoLabel1.Name = "FjobnoLabel1"
        FjobnoLabel1.Size = New System.Drawing.Size(48, 13)
        FjobnoLabel1.TabIndex = 20
        FjobnoLabel1.Text = "Enter JO"
        '
        'pd1
        '
        '
        'cmdPrint
        '
        Me.cmdPrint.Location = New System.Drawing.Point(367, 30)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(75, 23)
        Me.cmdPrint.TabIndex = 12
        Me.cmdPrint.Text = "&Print"
        Me.cmdPrint.UseVisualStyleBackColor = true
        '
        'DsM2M
        '
        Me.DsM2M.DataSetName = "dsM2M"
        Me.DsM2M.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'JomastBindingSource
        '
        Me.JomastBindingSource.DataMember = "jomast"
        Me.JomastBindingSource.DataSource = Me.DsM2M
        '
        'JomastTableAdapter
        '
        Me.JomastTableAdapter.ClearBeforeFill = true
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = false
        Me.TableAdapterManager.jomastTableAdapter = Me.JomastTableAdapter
        Me.TableAdapterManager.printer_xrefTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = HT_Quality_Tags.dsM2MTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'cbxJO
        '
        Me.cbxJO.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fjobno", true))
        Me.cbxJO.DataSource = Me.JomastBindingSource
        Me.cbxJO.DisplayMember = "fjobno"
        Me.cbxJO.FormattingEnabled = true
        Me.cbxJO.Location = New System.Drawing.Point(107, 59)
        Me.cbxJO.Name = "cbxJO"
        Me.cbxJO.Size = New System.Drawing.Size(121, 21)
        Me.cbxJO.TabIndex = 14
        '
        'txtJobName
        '
        Me.txtJobName.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fjob_name", true))
        Me.txtJobName.Location = New System.Drawing.Point(107, 86)
        Me.txtJobName.Name = "txtJobName"
        Me.txtJobName.ReadOnly = true
        Me.txtJobName.Size = New System.Drawing.Size(335, 20)
        Me.txtJobName.TabIndex = 15
        '
        'txtPartno
        '
        Me.txtPartno.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fpartno", true))
        Me.txtPartno.Location = New System.Drawing.Point(107, 112)
        Me.txtPartno.Name = "txtPartno"
        Me.txtPartno.ReadOnly = true
        Me.txtPartno.Size = New System.Drawing.Size(180, 20)
        Me.txtPartno.TabIndex = 16
        '
        'txtQty
        '
        Me.txtQty.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fquantity", true))
        Me.txtQty.Location = New System.Drawing.Point(107, 164)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.ReadOnly = true
        Me.txtQty.Size = New System.Drawing.Size(48, 20)
        Me.txtQty.TabIndex = 18
        '
        'txtDrawno
        '
        Me.txtDrawno.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fdrawno", true))
        Me.txtDrawno.Location = New System.Drawing.Point(107, 190)
        Me.txtDrawno.Name = "txtDrawno"
        Me.txtDrawno.ReadOnly = true
        Me.txtDrawno.Size = New System.Drawing.Size(75, 20)
        Me.txtDrawno.TabIndex = 19
        '
        'txtSO
        '
        Me.txtSO.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.JomastBindingSource, "fdescmemo", true))
        Me.txtSO.Location = New System.Drawing.Point(107, 138)
        Me.txtSO.Name = "txtSO"
        Me.txtSO.ReadOnly = true
        Me.txtSO.Size = New System.Drawing.Size(100, 20)
        Me.txtSO.TabIndex = 20
        '
        'txtJoSearch
        '
        Me.txtJoSearch.Location = New System.Drawing.Point(107, 33)
        Me.txtJoSearch.Name = "txtJoSearch"
        Me.txtJoSearch.Size = New System.Drawing.Size(100, 20)
        Me.txtJoSearch.TabIndex = 21
        '
        'chxAuto
        '
        Me.chxAuto.AutoSize = true
        Me.chxAuto.Location = New System.Drawing.Point(272, 35)
        Me.chxAuto.Name = "chxAuto"
        Me.chxAuto.Size = New System.Drawing.Size(72, 17)
        Me.chxAuto.TabIndex = 22
        Me.chxAuto.Text = "Auto Print"
        Me.chxAuto.UseVisualStyleBackColor = true
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AdminToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(530, 24)
        Me.MenuStrip1.TabIndex = 23
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DatabaseConnectionToolStripMenuItem, Me.SelectPrinterToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'DatabaseConnectionToolStripMenuItem
        '
        Me.DatabaseConnectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.M2MToolStripMenuItem, Me.ShowM2MConnToolStripMenuItem, Me.PackingSolutionToolStripMenuItem, Me.ShowPSConnToolStripMenuItem})
        Me.DatabaseConnectionToolStripMenuItem.Name = "DatabaseConnectionToolStripMenuItem"
        Me.DatabaseConnectionToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.DatabaseConnectionToolStripMenuItem.Text = "Reconfig DB"
        '
        'M2MToolStripMenuItem
        '
        Me.M2MToolStripMenuItem.Name = "M2MToolStripMenuItem"
        Me.M2MToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.M2MToolStripMenuItem.Text = "M2M"
        '
        'ShowM2MConnToolStripMenuItem
        '
        Me.ShowM2MConnToolStripMenuItem.Name = "ShowM2MConnToolStripMenuItem"
        Me.ShowM2MConnToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ShowM2MConnToolStripMenuItem.Text = "Show M2M Conn"
        '
        'PackingSolutionToolStripMenuItem
        '
        Me.PackingSolutionToolStripMenuItem.Name = "PackingSolutionToolStripMenuItem"
        Me.PackingSolutionToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.PackingSolutionToolStripMenuItem.Text = "Packing Solution"
        '
        'ShowPSConnToolStripMenuItem
        '
        Me.ShowPSConnToolStripMenuItem.Name = "ShowPSConnToolStripMenuItem"
        Me.ShowPSConnToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.ShowPSConnToolStripMenuItem.Text = "Show PS Conn"
        '
        'SelectPrinterToolStripMenuItem
        '
        Me.SelectPrinterToolStripMenuItem.Name = "SelectPrinterToolStripMenuItem"
        Me.SelectPrinterToolStripMenuItem.Size = New System.Drawing.Size(143, 22)
        Me.SelectPrinterToolStripMenuItem.Text = "Select Printer"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'lblPrinter
        '
        Me.lblPrinter.AutoSize = true
        Me.lblPrinter.Location = New System.Drawing.Point(11, 224)
        Me.lblPrinter.Name = "lblPrinter"
        Me.lblPrinter.Size = New System.Drawing.Size(82, 13)
        Me.lblPrinter.TabIndex = 24
        Me.lblPrinter.Text = "Selected Printer"
        '
        'lblSelectedPrinter
        '
        Me.lblSelectedPrinter.AutoSize = true
        Me.lblSelectedPrinter.Location = New System.Drawing.Point(107, 224)
        Me.lblSelectedPrinter.Name = "lblSelectedPrinter"
        Me.lblSelectedPrinter.Size = New System.Drawing.Size(54, 13)
        Me.lblSelectedPrinter.TabIndex = 25
        Me.lblSelectedPrinter.Text = "No Printer"
        '
        'frmQualityTags
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(530, 246)
        Me.Controls.Add(Me.lblSelectedPrinter)
        Me.Controls.Add(Me.lblPrinter)
        Me.Controls.Add(Me.chxAuto)
        Me.Controls.Add(FjobnoLabel1)
        Me.Controls.Add(Me.txtJoSearch)
        Me.Controls.Add(FdescmemoLabel)
        Me.Controls.Add(Me.txtSO)
        Me.Controls.Add(FdrawnoLabel)
        Me.Controls.Add(Me.txtDrawno)
        Me.Controls.Add(FquantityLabel)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(FpartnoLabel)
        Me.Controls.Add(Me.txtPartno)
        Me.Controls.Add(Fjob_nameLabel)
        Me.Controls.Add(Me.txtJobName)
        Me.Controls.Add(FjobnoLabel)
        Me.Controls.Add(Me.cbxJO)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = false
        Me.Name = "frmQualityTags"
        Me.Text = "HT Quality Tags"
        CType(Me.DsM2M,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.JomastBindingSource,System.ComponentModel.ISupportInitialize).EndInit
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents pd1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents DsM2M As HT_Quality_Tags.dsM2M
    Friend WithEvents JomastBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents JomastTableAdapter As HT_Quality_Tags.dsM2MTableAdapters.jomastTableAdapter
    Friend WithEvents TableAdapterManager As HT_Quality_Tags.dsM2MTableAdapters.TableAdapterManager
    Friend WithEvents cbxJO As System.Windows.Forms.ComboBox
    Friend WithEvents txtJobName As System.Windows.Forms.TextBox
    Friend WithEvents txtPartno As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents txtDrawno As System.Windows.Forms.TextBox
    Friend WithEvents txtSO As System.Windows.Forms.TextBox
    Friend WithEvents txtJoSearch As System.Windows.Forms.TextBox
    Friend WithEvents chxAuto As System.Windows.Forms.CheckBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DatabaseConnectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents M2MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShowM2MConnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SelectPrinterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblPrinter As Label
    Friend WithEvents lblSelectedPrinter As Label
    Friend WithEvents PackingSolutionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowPSConnToolStripMenuItem As ToolStripMenuItem
End Class
