﻿Imports System.Reflection

Public Class frmQualityTags
#Region "< Members >"
    Dim sFullJoName As String
    Dim sPrinter As String
    private _m2mCatalog as String
    private _psCatalog as String
    private sUser As string
#End Region

    Private Sub JomastBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.JomastBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DsM2M)

    End Sub

    Private Sub frmQualityTags_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        My.Settings.Save()
    End Sub

    Private Sub frmQualityTags_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dim appSetting = new AppSettings()

        dim sqlHelper = new SqlHelper(appSetting.GetConnectionString("M2M"))
        If (Not sqlHelper.IsConnected)
            dim frm = New frmSqlConfig
            frm.Title = "M2M"
            frm.ShowDialog(me)
        End If
        dim psSqlHelper = new SqlHelper(appSetting.GetConnectionString("PS"))
        If (Not psSqlHelper.IsConnected)
            dim frm = New frmSqlConfig
            frm.Title = "PS"
            frm.ShowDialog(me)
        End If
        'TODO: This line of code loads data into the 'DsM2M.jomast' table. You can move, or remove it, as needed.
        Me.JomastTableAdapter.FillBy(Me.DsM2M.jomast)
        Dim margins As New Printing.Margins(0, 0, 0, 0)
        pd1.DefaultPageSettings.Margins = margins

        txtJoSearch.Focus()

        appSetting.Refersh()

        TitleReset()
        sUser = Environment.UserName
        SetPrinter()
    End Sub
    Private Sub pd1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles pd1.PrintPage
        Dim g As Graphics = e.Graphics
        ' g.PageUnit = GraphicsUnit.Inch

        Dim clientRectangle As New Rectangle(0, 0, Width, Height)
        Dim blkPen As New Pen(Color.Black, 4)
        Dim point1 As New Point(20, 315)
        Dim point2 As New Point(580, 315)
        Dim thinPen As New Pen(Color.Black, 2)
        Dim qc1box As New Rectangle(150, 20, 40, 75)
        Dim qc2box As New Rectangle(150, 405, 40, 75)


        Dim style As New StringFormat
        style.Alignment = StringAlignment.Center
        style.FormatFlags = StringFormatFlags.DirectionVertical
        Dim nQuarter As Integer = 3

        Dim jobNameFont As New Font("Arial Narrow", 0.3 * 100, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim StockFont As New Font("Arial Black", 0.4 * 100, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim jobnoFont As New Font("Arial", 0.3 * 100, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim pcsFont As New Font("Arial", 0.2 * 100, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim barFont As New Font("BC C39 3/1 Medium", 0.9 * 100, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim dToday As Date = Now

        Dim sPartno As String = txtPartno.Text
        Dim sJobName As String = txtJobName.Text
        Dim sSO As String = txtSO.Text
        Dim sJobNo As String = cbxJO.Text
        Dim nQty As Integer = txtQty.Text
        Dim sDrawno As String = txtDrawno.Text

        sJobNo = Mid(sJobNo, 1, 5)

        g.DrawString(sSO, jobnoFont, Brushes.Black, 250, 250, style)
        g.DrawString(sJobName, jobNameFont, Brushes.Black, 220, 250, style)
        g.DrawString(sPartno, jobnoFont, Brushes.Black, 175, 250, style)
        g.DrawString(sDrawno, jobnoFont, Brushes.Black, 145, 250, style)
        g.DrawString("QC", pcsFont, Brushes.Black, 190, 55, style)
        g.DrawString("Qty", pcsFont, Brushes.Black, 190, 435, style)
        g.DrawString("*" & sJobNo & "*", barFont, Brushes.Black, 50, 250, style)
        g.DrawString(sJobNo, StockFont, Brushes.Black, 0, 250, style)
        g.DrawRectangle(thinPen, qc1box)
        g.DrawRectangle(thinPen, qc2box)

    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        'pd1.Print()
        DirectToPrint()

    End Sub

    Private Sub JomastBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.JomastBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DsM2M)

    End Sub
    Private Sub DirectToPrint()
        Dim rpt As New rptHTQC
        Dim sPartno As String = txtPartno.Text
        Dim sJobName As String = txtJobName.Text
        Dim sSO As String = txtSO.Text
        Dim sJobNo As String = cbxJO.Text
        Dim nQty As Integer = txtQty.Text
        Dim sDrawno As String = txtDrawno.Text

        rpt.FileName = "\\rrs0009\public\RRPRS\Packing Solutions\crystal reports\rptHTQC.rpt"
        rpt.SetParameterValue("fsono", sSO)
        rpt.SetParameterValue("fjob_name", sJobName)
        rpt.SetParameterValue("fpartno", sPartno)
        rpt.SetParameterValue("fdrawno", sDrawno)
        rpt.SetParameterValue("fjobno", sJobNo)

        'Dim outputfile As String
        'outputfile = "\\rrs0005\m2msql50\caginc\uploads\packinglist.xls"
        'rpt.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, outputfile)
        Try
            rpt.PrintOptions.PrinterName = sPrinter
            rpt.PrintToPrinter(1, False, 0, 0) ' 05/22/2012 JRM - print one tag at a time.
        Catch ex As Exception
            MsgBox("Print failed", MsgBoxStyle.Exclamation)
            Using taError As New dsM2MTableAdapters.QueriesTableAdapter
                taError.insAppErrors("??", Me.GetType.Name, ex.Message.ToString, System.Environment.UserName)
            End Using
        End Try
        rpt.Dispose()

    End Sub

    Private Sub txtJoSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtJoSearch.TextChanged
        JomastBindingSource.Filter = "fjobno like '" & txtJoSearch.Text.Trim & "%'"
        If JomastBindingSource.Count > 0 Then
            If JomastBindingSource.Count = 1 Then
                'MsgBox("time to print")
                If chxAuto.Checked = True Then
                    'pd1.PrinterSettings.Copies = 1
                    'pd1.Print()
                    DirectToPrint()
                    txtJoSearch.SelectAll()
                End If
            End If
        Else
            MsgBox(txtJoSearch.Text.Trim.ToUpper & " not found", MsgBoxStyle.Information, "JO not open")
        End If
    End Sub

    Private Sub M2MToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M2MToolStripMenuItem.Click
        dim frm = New frmSqlConfig
        frm.Title = "M2M"
        frm.ShowDialog(me)
        If frm.Saved Then
            TitleReset()
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem.Click
        Dim obj As New About
        obj.ShowDialog(Me)
        obj.Dispose()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        End
    End Sub

    Private Sub ShowM2MConnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowM2MConnToolStripMenuItem.Click
        dim appSettings = New AppSettings()
        dim connStr = appSettings.GetConnectionString("M2M")
        Messagebox.Show(connStr)
    End Sub


    private Sub TitleReset
        dim appSetting = new AppSettings()
        appSetting.Refersh()

        Dim connM2M as string() = appSetting.GetConnectionString("M2M").Split(";")
        Dim connPS as string() = appSetting.GetConnectionString("PS").Split(";")
        _m2mCatalog = GetSource(connM2M, _m2mCatalog)
        _psCatalog = GetSource(connPS, _psCatalog)
        Text = "HT Quality Tags v." + Assembly.GetExecutingAssembly().GetName().Version.ToString() + "     M2M: " + _m2mCatalog + " PS: " + _psCatalog

    End Sub
    Private Function GetSource(sConnArr As String(), dSource As String) As String

        For Each el As String In sConnArr
            Dim aEl As String() = el.Split("=")
            If el.Contains("Catalog") Then
                dSource = aEl(1)
                exit for
            End If
        Next
        Return dSource
    End Function

    Private Sub SelectPrinterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SelectPrinterToolStripMenuItem.Click
        Dim obj As New frmSetPrinter

        If DialogResult.OK = obj.ShowDialog(Me) Then
            Using ta As New dsM2MTableAdapters.printer_xrefTableAdapter
                Dim dt As New dsM2M.printer_xrefDataTable
                dt = ta.GetDataByFacCellUser("default", "TC-DC", sUser)
                if (dt.Count > 0)
                    ' update
                    dim aKey() As string = {"Default", "TC-DC", sUser}
                    dim dr as dsM2M.printer_xrefRow = dt.Rows.Find(aKey)
                    dr.quality_tag = obj.sPrinter
                    ta.Update(dr)
                else
                    dt.Addprinter_xrefRow("Default", "TC-DC", sUser, "", "", "", Now, "HT", obj.sPrinter)
                End If
                Try
                    ta.Update(dt)
                Catch ex As Exception
                    using taError As New dsM2MTableAdapters.QueriesTableAdapter
                        taError.insAppErrors("Default","QC Tag",ex.Message.ToString(),sUser)
                        End Using
                    MsgBox("Set Printer_Xref - " & ex.Message.ToString())
                End Try

            End Using
            sPrinter = obj.sPrinter
            pd1.PrinterSettings.PrinterName = obj.sPrinter
            lblSelectedPrinter.Text = obj.sPrinter
        End If
        obj.Dispose()
        SetPrinter()
    End Sub
    Private Sub SetPrinter()
        If (sUser = "")
            return
        End If
        Using ta As New dsM2MTableAdapters.printer_xrefTableAdapter
            dim dt as dsM2M.printer_xrefDataTable
            dt =  ta.GetDataByFacCellUser("default", "TC-DC", sUser)
            if (dt.Count > 0)
                sPrinter = dt.Rows(0).Item("quality_tag")
                pd1.PrinterSettings.PrinterName = dt.Rows(0).Item("quality_tag")
                lblSelectedPrinter.Text = dt.Rows(0).Item("quality_tag")
            End If

        End Using

    End Sub

    Private Sub PackingSolutionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PackingSolutionToolStripMenuItem.Click
        dim frm = New frmSqlConfig
        frm.Title = "PS"
        frm.ShowDialog(me)
        If frm.Saved Then
            TitleReset()
        End If
    End Sub

    Private Sub ShowPSConnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowPSConnToolStripMenuItem.Click
        dim appSettings = New AppSettings()
        dim connStr = appSettings.GetConnectionString("PS")
        Messagebox.Show(connStr)
    End Sub
End Class
