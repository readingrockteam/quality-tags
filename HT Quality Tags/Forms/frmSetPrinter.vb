﻿Imports System.Windows.Forms

Public Class frmSetPrinter
    public sPrinter as string
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmSetPrinter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For each printer as string in System.Drawing.Printing.PrinterSettings.InstalledPrinters
            cbxPrinters.Items.Add(printer)
        Next printer
    End Sub

    Private Sub cbxPrinters_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbxPrinters.SelectionChangeCommitted
        sPrinter = cbxPrinters.SelectedItem
    End Sub
End Class
